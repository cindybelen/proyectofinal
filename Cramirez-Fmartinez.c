//Cindy Ramirez Vivanco    Rut: 18.686.782-3
//Facundo Martinez Gulle   Rut: 20.343.124-4

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "libreria.h"

//Definicion de funciones 
void Consultar_por_carrera();
int Simular_ponderacion();
int Consultar_por_facultad();


void RECORRER_ARCHIVO(char path[],PONDERACIONES datos[]){
//Funcion para mostrar lo que esta dentro del archivo txt
    FILE *arch;//Se define nombre de archivo
    int i=0;//Declaracion variable
    char carrera;
 if((arch=fopen(path,"r"))==NULL){//Abre el archivo en modo lectura
        printf("\nError\n.");
        exit(0);//Sale del codigo totalmente
    }
    else{
        for (i=0;i<74;i++){//Mientras no lea el final del archivo se ejecutara esto
            fscanf(arch,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",&datos[i].facultad,&datos[i].carrera,&datos[i].codigo_carrera,&datos[i].NEM,&datos[i].RANK,&datos[i].LENG,&datos[i].MAT,&datos[i].HIST,&datos[i].CS,&datos[i].POND,&datos[i].PSU1,&datos[i].MAX,&datos[i].MIN,&datos[i].PSU,&datos[i].BEA);
        }
        fclose(arch); //Se cierra el archivo
    }

}
//Funcion que lee el menu
void menu(PONDERACIONES datos[]){
//Pedira ingresar una opcion para luego lo lea en un switch y desde ahi llamar a la funcion que se necesite
    int opc;//Identificando variable
    do{
        printf( "\n Ingrese operacion que desea realizar: " );
        printf( "\n   1. Consultar ponderacion carrera" );
        printf( "\n   2. Simular postulacion carrera" );
        printf( "\n   3. Mostrar ponderaciones facultad");
        printf( "\n   4. Salir\n");
        scanf( "%d", &opc );//Ingresa variable de lo que necesita hacer
        switch ( opc ){
            case 1: Consultar_por_carrera(datos);
                break;
            case 2: Simular_ponderacion("carreras.txt",datos);
                break;
            case 3: Consultar_por_facultad(datos);
                break;
    }
}
while ( opc != 4 );
}

//
void Consultar_por_carrera(PONDERACIONES datos[]){
    FILE *arch; //Identifica el archivo que se usara
    char carrera,var; //Identificacion de variables
    int i;
    arch=fopen("carreras.txt","r"); //Identifica variable que abre el archivo para luego poder usarlo
    printf("Ingresa la carrera que deseas simular: (en minuscula)\n");
    scanf("%s", &carrera);
    for(i=0;i<74;i++){
        strcpy(var,datos[i].carrera);//Copia la linea en la variable var
        var=strtok(var,",");//Corta la linea cuando encuentra la , y la guarda nuevamente en var
        if (strcmp(var,carrera)==0){//Compara lo guardado en var con lo ingresado si esto coincide se mostrara
            printf("%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",datos[i].facultad,datos[i].carrera,datos[i].codigo_carrera,datos[i].NEM,datos[i].RANK,datos[i].LENG,datos[i].MAT,datos[i].HIST,datos[i].CS,datos[i].POND,datos[i].PSU1,datos[i].MAX,datos[i].MIN,datos[i].PSU,datos[i].BEA);//Muestra en pantalla lo qe esta dentro del archivo
        }
    }
    fclose(arch);//Se cierra archivo
}


int Simular_ponderacion(char path[],PONDERACIONES datos[]){
    return 0;
}


int Consultar_por_facultad(PONDERACIONES datos[]){
    FILE *arch;//Identifica el archivo
    int i; //Identificacion de variables
    char FACULTAD[50];
    arch=fopen("carreras.txt","r");//Abre el archivo en modo lectura
    printf("Ingrese nombre facultad (cambiar los espacion por '-') : \n");
    scanf("%s", &FACULTAD);//Recibe el nombre que se desea buscar
    for(i=0;i<74;i++){//Recorre la lista para poder buscar
        if(strcmp(FACULTAD,datos[i].facultad)==0);{ //Compara la linea guardada con la facultad
            printf("%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",datos[i].facultad,datos[i].carrera,datos[i].codigo_carrera,datos[i].NEM,datos[i].RANK,datos[i].LENG,datos[i].MAT,datos[i].HIST,datos[i].CS,datos[i].POND,datos[i].PSU1,datos[i].MAX,datos[i].MIN,datos[i].PSU,datos[i].BEA); //Muestra en pantalla 
        }
    }
    fclose(arch);//Cierra el archivo
    return 0;
}
   
int main(){
    system("clear");//Limpia la pantalla
    PONDERACIONES datos[1000];//Declara el arreglo
    RECORRER_ARCHIVO("carreras.txt",datos); //Llama a la funcion para recorrer el archivo
    menu(datos);//Llama al menu para recibir la orden que se desea ejecutar
    return  0;
}
