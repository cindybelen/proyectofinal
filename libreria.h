#ifndef PONDERADO_H
#define PONDERADO_H
typedef struct universidad{
    char facultad[60];
    char carrera[50];
    char codigo_carrera[6];
    char NEM[4];
    char RANK[4];
    char LENG[4];
    char MAT[4];
    char HIST[4];
    char CS[4];
    char POND[4];
    char PSU1[4];
    char MAX[4];
    char MIN[4];
    char PSU[4];
    char BEA[4];
} PONDERACIONES;
#endif
